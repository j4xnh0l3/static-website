## Generic Pages for MongoDB

These pages can be extended to provide a minimal, power-user experience
for endpoints that do simple CRUD operations on MongoDB.
(power-user == with great power (to destroy things) comes great responsibility)

Note that you will want to use authentications and SSL if untrusted users
can hit these pages.
