#!/usr/bin/env python

import os
import shutil
from jinja2 import (
    BaseLoader,
    Environment,
    TemplateNotFound,
)


ROOT_DIR = os.path.dirname(
    os.path.abspath(__file__),
)


class Loader(BaseLoader):
    def __init__(self, path):
        self.path = path

    def get_source(
        self,
        environment,
        template,
    ):
        path = os.path.join(
            self.path,
            template,
        )
        if not os.path.exists(path):
            raise TemplateNotFound(template)
        mtime = os.path.getmtime(path)
        with open(path, 'r') as f:
            source = f.read()
        return (
            source,
            path,
            lambda: mtime == os.path.getmtime(path),
        )


class Templates(object):
    def __init__(
        self,
        templates,
        static_dir='static',
        dist_dir='dist',
        root_dir=ROOT_DIR
    ):
        self.templates = templates
        self.static_dir = static_dir
        self.dist_dir = dist_dir
        self.root_dir = root_dir

    def clean(self, dirname):
        if os.path.exists(dirname):
            shutil.rmtree(dirname)

    def build(self, **kwargs):
        dist_dir = os.path.join(
            self.root_dir,
            self.dist_dir,
        )
        self.clean(dist_dir)
        os.mkdir(dist_dir)

        static_dir = os.path.join(
            dist_dir,
            self.static_dir,
        )
        print("Copying static directory")
        shutil.copytree(
            os.path.join(
                self.root_dir,
                self.static_dir,
            ),
            static_dir,
        )

        favicon = os.path.join(
            static_dir,
            'favicon.ico',
        )

        if os.path.isfile(favicon):
            print("copying favicon.ico to /")
            shutil.copy(favicon, dist_dir)

        env = Environment(
            loader=Loader(
                self.root_dir,
            ),
        )
        print("Rendering:")
        for template in self.templates:
            tmpl = env.get_template(template)
            tmpl = tmpl.render(**kwargs)
            path = template.split(os.sep, 1)[1]
            dirname = os.path.join(
                dist_dir,
                os.path.dirname(path),
            )

            if (
                dirname
                and
                not os.path.isdir(dirname)
            ):
                os.makedirs(dirname)

            html = os.path.join(
                dist_dir,
                path,
            )
            print("    " + path)
            with open(html, 'w') as f:
                f.write(tmpl)

        return True


def main():
    os.chdir(ROOT_DIR)
    assert os.path.isdir('src')
    templates = [
        os.path.join(x, z)
        for x, _, y in os.walk('src')
        for z in y
        if not z.endswith('.swp')
    ]
    t = Templates(
        templates=templates,
    )
    t.build()
    print("\nStatic site generated in:\n")
    print(
        os.path.join(
            ROOT_DIR,
            t.dist_dir,
        )
    )


if __name__ == "__main__":
    main()
