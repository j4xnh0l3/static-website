
function onSearch(api_url, http_method, params){
    requestCodeMirror.setValue(
        decodeURIComponent(params)
    );

    $.ajax({
        type: http_method,
        url: api_url,
        data: params,
        success: function(j, textStatus, jqXHR){
            responseCodeMirror.setValue(
                JSON.stringify(j, null, 2)
            );
            $('#alert').removeClass();
            $('#alert').addClass(
                "alert alert-success col-sm-offset-1 col-sm-11"
            );
            $('#alert').html(
                decodeURIComponent(
                    [
                        "Text Status: " + textStatus
                    ].join("<br />")
                )
            );

            $("table tbody").empty();

            j.forEach(function(row){
                markup = rowToMarkup(row);
                $("table tbody").append(markup);
            });

            var browser_url = window.location.href.split("?", 1)[0];

            window.history.pushState(
                null,
                null,
                browser_url + "?" + params
            );
        },
        error: function(xhr, ajaxOptions, thrownError){
            $('#alert').removeClass();
            $('#alert').addClass(
                "alert alert-danger col-sm-offset-1 col-sm-11"
            );
            $('#alert').html(
                decodeURIComponent(
                    [
                        "Thrown Error: " + thrownError,
                        "Response Text: " + xhr.responseText
                    ].join("<br />")
                )
            );
            responseCodeMirror.setValue("");
        },
        //dataType: "json",
        contentType : "application/json"
    });
}
