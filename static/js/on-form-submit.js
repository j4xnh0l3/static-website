
$('#my-form').submit(function(event){
    event.preventDefault();

    var formData = formToJSON(); // defined by template

    requestCodeMirror.setValue(
        JSON.stringify(
            $.parseJSON(formData),
            null,
            "\t"
        )
    );

    $.ajax({
        type: API_METHOD, // defined by template
        url: API_ENDPOINT, // defined by template
        data: formData,
        success: function(j, textStatus, jqXHR){
            responseCodeMirror.setValue(
                JSON.stringify(j, null, 2)
            );
            $('#alert').removeClass();
            $('#alert').addClass(
                "alert alert-success col-sm-offset-1 col-sm-11"
            );
            $('#alert').html(
                "Text Status: " + textStatus
            );
            /*
            $('input, select, textarea').each(
                function(index){
                    var input = $(this);
                    input.val("");
                }
            );
            */
        },
        error: function(xhr, ajaxOptions, thrownError){
            $('#alert').removeClass();
            $('#alert').addClass(
                "alert alert-danger col-sm-offset-1 col-sm-11"
            );
            $('#alert').html(
                decodeURIComponent(
                    [
                        "Thrown Error: " + thrownError,
                        "Response Text: " + xhr.responseText
                    ].join("<br />")
                )
            );
            responseCodeMirror.setValue("");
        },
        dataType: "json",
        contentType : "application/json"
    });
});
